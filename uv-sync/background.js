chrome.bookmarks.onCreated.addListener(function (message, sender, sendResponse) {
    changeBookMarks()
});
chrome.bookmarks.onChanged.addListener(function (message, sender, sendResponse) {
    changeBookMarks()
});
chrome.bookmarks.onRemoved.addListener(function (message, sender, sendResponse) {
    changeBookMarks()
});
chrome.bookmarks.onChildrenReordered.addListener(function (message, sender, sendResponse) {
    changeBookMarks()
});
chrome.bookmarks.onImportEnded.addListener(function (message, sender, sendResponse) {
    changeBookMarks()
});
chrome.bookmarks.onMoved.addListener(function (message, sender, sendResponse) {
    changeBookMarks()
});


chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    console.log(request)
    if (request.target === "background") {
        console.log("background received: ", request);
        if (request.act === 'loginToken') {
            DataKeeper.setData('token', request.data)
        } else if (request.act === 'setProcess') {
            DataKeeper.setData('process', request.data)
        } else if (request.act === 'getProcess') {
            DataKeeper.getData('process').then(re=>{
                chrome.runtime.sendMessage({ target: "popup",act:'ackProcess',data:re.process });
            })
        } else if (request.act === 'getHash') {
            DataKeeper.getData('last').then(re=>{
                chrome.runtime.sendMessage({ target: "popup",act:'ackHash',data:re.last });
            })
        } else if (request.act === 'updateHash') {
            writeHash(request.data)
        } else if (request.act === 'logoutToken') {
            DataKeeper.removeData('token')
            DataKeeper.removeData('last')
            DataKeeper.setData('process', "0")
        }
    }
})


// ======================== 工具 ===========================
var DataKeeper = {
    setData: function (k, v) {
        var tmp = {}
        tmp[k] = v
        chrome.storage.local.set(tmp);
    },
    getData: function (k) {
        return chrome.storage.local.get(k)
    },
    removeData: function (k) {
        chrome.storage.local.remove(k);
    }
};
var PanUtil = {
    dateFormat: {
        format: function (date, pattern) {
            var date = date;
            if (isNaN(date.getTime()) || date.getTime() === new Date(null).getTime())
                return "~";
            var defaults = [
                { key: "yyyy", value: date.getFullYear() },
                { key: "yy", value: date.getYear() },
                { key: "MM", value: date.getMonth() + 1 },
                { key: "dd", value: date.getDate() },
                { key: "HH", value: date.getHours() },
                { key: "mm", value: date.getMinutes() },
                { key: "ss", value: date.getSeconds() },
                { key: "SSS", value: date.getMilliseconds() },
                {
                    key: "hh",
                    value: function (re, key, date) {
                        if (date.getHours() > 12) {
                            return re.replace("hh", ("00" + (date.getHours() - 12)).substring(2 + (date.getHours() - 12).toString().length - 2)) + " PM";
                        } else {
                            return re.replace("hh", ("00" + date.getHours()).substring(2 + date.getHours().toString().length - 2)) + " AM";
                        }
                    }
                }
            ];
            var returns = pattern;
            for (var i = 0; i < defaults.length; i++) {
                var it = defaults[i];
                if (pattern.indexOf(it.key) >= 0) {
                    if (typeof (it.value) === "function") {
                        var _f = it.value;
                        returns = _f(returns, it.key, date);
                    } else
                        returns = returns.replace(new RegExp(it.key, "g"), ("00" + it.value).substring(2 + it.value.toString().length - it.key.length));
                }
            }
            return returns;
        },
        parse: function (str, pattern) {
            var year = 1900,
                month = 0,
                day = 1,
                hours = 0,
                minutes = 0,
                seconds = 0,
                milliseconds = 0;
            if (pattern.indexOf("yyyy") >= 0) {
                year = Number(str.substr(pattern.indexOf("yyyy"), 4));
            } else if (pattern.indexOf("yy") >= 0) {
                year = Number(str.substr(pattern.indexOf("yy"), 2)) + 100 + 1900;
            }
            if (pattern.indexOf("MM") >= 0) {
                month = Number(str.substr(pattern.indexOf("MM"), 2)) - 1;
            }
            if (pattern.indexOf("dd") >= 0) {
                day = Number(str.substr(pattern.indexOf("dd"), 2));
            }
            if (pattern.indexOf("hh") >= 0) {
                hours = Number(str.substr(pattern.indexOf("hh"), 2)) + Number(str.indexOf("AM") >= 0 ? 0 : (str.indexOf("PM") >= 0 ? 12 : 0));
            }
            if (pattern.indexOf("HH") >= 0) {
                hours = Number(str.substr(pattern.indexOf("HH"), 2));
            }
            if (pattern.indexOf("mm") >= 0) {
                minutes = Number(str.substr(pattern.indexOf("mm"), 2));
            }
            if (pattern.indexOf("ss") >= 0) {
                seconds = Number(str.substr(pattern.indexOf("ss"), 2));
            }
            if (pattern.indexOf("SSS") >= 0) {
                milliseconds = Number(str.substr(pattern.indexOf("SSS"), 3));
            }
            var _re = new Date(year, month, day, hours, minutes, seconds, milliseconds);
            return isNaN(_re) && "~" || _re;
        }
    },
    parseObjectToFormData: function (obj) {
        var search = "";
        for (var k in obj) {
            search += "&" + k + "=" + encodeURIComponent(obj[k]);
        }
        return search.substring(1);
    }
}
var $ = {
    ajax: function (cfg) {
        // $.ajax({
        //     url: url,
        //     data: data,
        //     type: type,
        //     success: function(re,xhr){},
        //     dataType: dataType,
        //     error: function (xhr, status, error) {}
        // });
        if ((cfg.type || '').toUpperCase() === 'GET') {
            $.get(cfg.url, cfg.data, cfg.success, cfg, cfg.error)
        } else if ((cfg.type || '').toUpperCase() === 'POST') {
            $.postForm(cfg.url, cfg.data, cfg.success, cfg, cfg.error)
        }
    },
    get: function (url, paramsMap, callback, preConfig, error) {
        if (paramsMap) {
            if (url.indexOf("?") < 0) {
                url += '?'
            }
            url += PanUtil.parseObjectToFormData(paramsMap)
        }
        fetch(url, {
            method: "GET", // *GET, POST, PUT, DELETE, 等
            mode: "cors", // no-cors, *cors, same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
                // "Content-Type": "application/json",
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: "follow", // manual, *follow, error
            referrerPolicy: "no-referrer", // no-referrer, *client
            // body: PanUtil.parseObjectToFormData(paramsMap), // body 数据类型必须与 "Content-Type" 头相匹配
        })
            .then(res => {
                if ((preConfig.dataType || '').toUpperCase() === 'JSON') {
                    return res.json()
                } else {
                    return res.text()
                }
            })
            .then(res => {
                callback.call(this, res, null);
            })
    },
    postForm: function (url, paramsMap, callback, preConfig, error) {
        fetch(url, {
            method: "POST", // *GET, POST, PUT, DELETE, 等
            mode: "cors", // no-cors, *cors, same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
                'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8",
            },
            redirect: "follow", // manual, *follow, error
            referrerPolicy: "no-referrer", // no-referrer, *client
            body: PanUtil.parseObjectToFormData(paramsMap), // body 数据类型必须与 "Content-Type" 头相匹配
        })
            .then(res => {
                if ((preConfig.dataType || '').toUpperCase() === 'JSON') {
                    return res.json()
                } else {
                    return res.text()
                }
            })
            .then(res => {
                callback.call(this, res, null);
            })
    },
    postJson: function (url, paramsMap, callback, preConfig, error) {
        var obj = new XMLHttpRequest();
        obj.open("POST", url, true);
        obj.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
        preConfig && preConfig(obj);
        obj.onreadystatechange = function () {
            if (obj.readyState === 4 && (obj.status === 200 || obj.status === 304)) {
                var json = NaN
                try {
                    json = JSON.parse(obj.responseText)
                } catch (e) {
                    console.error(e)
                }
                callback.call(this, json, obj);
            }
        };
        obj.onerror = function (e) {
            error && error.call(this, obj, obj.status, e)
        }
        obj.send(JSON.stringify(paramsMap));
    }

}
function panAjax(type, url, data, succ, dataType, idx) {
    onLine(function (flag) {
        if (flag) {
            idx = idx || 1;
            $.ajax({
                url: url,
                data: data,
                type: type,
                // headers:{'Set-Cookie':'widget_session=abc123; SameSite=None; Secure'},
                success: succ,
                dataType: dataType,
                error: function (xhr, status, error) {
                    console.info("request retry again,due to : ", error)
                    document.getElementById("testIframe").src = url;
                    if (idx < 5) {
                        setTimeout(function () {
                            idx++;
                            panAjax(type, url, data, succ, dataType, idx);
                        }, 1000);
                    } else {
                        console.info("request 5th failed, no try again!");
                        DataNotify.notify("同步数据请求已失败5次！！！", urlRoot + URL_TREE);
                    }
                }
            });
        } else {
            console.error("网络异常")
        }
    })
}
function onLine(callback) {
    $.ajax({
        url: 'https://www.baidu.com/favicon.ico?_t=' + Date.now(),
        type: 'GET',
        success: function () {
            if (callback) callback(true)
        },
        error: function () {
            if (callback) callback(false)
        }
    })
}
// ======================== 业务 ===========================
var timeout
var doWork = false
// var urlRoot = "http://pan.is-great.net:80";
// var urlRoot = "http://localhost:80";
var urlRoot = "http://localhost:5681";
// var urlRoot = "http://175.24.181.214:5681";
// var urlRoot = PanConfig.URL_ROOT;

var URL_APIROOT = "/UVSync/backend/gateway.php";
var URL_TREE = URL_APIROOT + "?m=BookMarkController!getBookMarkList"
var URL_PAGE = URL_APIROOT + "?m=BookMarkController!getBookMarkHistory"
// $.ajax({
//     url: "https://gitee.com/owendawn/Resource/raw/master/panbase.js",
//     data: {},
//     async: false,
//     success: function (re) {
//         re = re.replace('var PanConfig=', '').replace(/\s\/\/.+\n/g, '').replace(/\s/g, '')
//         var config = JSON.parse(re)
//         urlRoot = config.URL_ROOT
//         console.log('urlRoot', urlRoot)
//     },
//     dataType: "text"
// })
fetch("https://gitee.com/owendawn/Resource/raw/master/panbase.js", {
    method: 'GET'
})
    .then(res => {
        // console.log(res,res.text())
        return res.text()
    })
    .then(re => {
        re = re.replace('var PanConfig=', '').replace(/\s\/\/.+\n/g, '').replace(/\s/g, '')
        var config = JSON.parse(re)
        urlRoot = config.URL_ROOT
        console.log('urlRoot', urlRoot)
    })

var changeBookMarks = function (id, data) {
    if (timeout !== null) {
        clearTimeout(timeout);
        timeout = null;
    }
    timeout = setTimeout(function () {
        writeHash(PanUtil.dateFormat.format(new Date(), 'yyyy-MM-dd HH:mm:ss'));
        sychrosize();
        timeout = null;
    }, 0);
}

function writeHash(hash) {
    DataKeeper.setData("last", hash);
    DataKeeper.getData("last").then(re => {
        console.log('更新书签：', re);
    })

}

function sychrosize() {
    servercheck(function (callBack) {
        DataKeeper.getData(["token", 'last']).then(cfg => {
            // console.log(cfg)
            chrome.bookmarks.getTree(function (tree) {
                panAjax(
                    "post",
                    urlRoot + URL_TREE, {
                    token: cfg.token,
                    hash: cfg.last
                },
                    function (re) {
                        callBack();
                        if (re.code === 200 && re.needPush) {
                            var hash = cfg.last;
                            panAjax(
                                "post",
                                urlRoot + URL_APIROOT + "?m=BookMarkController!addBookMarkLog",
                                {
                                    token: cfg.token,
                                    hash: hash,
                                    bookmarks: JSON.stringify(tree)
                                },
                                function (re) {
                                    console.info(hash, re)
                                    if (re.code === 200 && re.updated) {
                                        writeHash(hash);
                                        if (timeout !== null) {
                                            clearTimeout(timeout);
                                            timeout = null;
                                        }
                                    } else if (re.code === 500) {
                                        if (re.info) {
                                            console.error(re.info);
                                        }
                                    }
                                },
                                "json"
                            );
                        }
                    },
                    "json"
                );
            });
        })
    });
}




function servercheck(callback, idx) {
    idx = idx || 1;
    $.ajax({
        url: urlRoot + "/UVSync/backend/alive.html",
        data: {},
        type: 'get',
        // headers:{'Set-Cookie':'widget_session=abc123; SameSite=None; Secure'},
        success: function (re, textStatus, request) {
            // console.log(request);
            re === "hi" && console.info("uv-sync server say \"%s\" to you", re);
            if (!doWork) {
                DataKeeper.setData("do", "true");
                doWork = true;
                callback && callback(function () {
                    DataKeeper.setData("do", "false");
                    doWork = false;
                });
            }
        },
        error: function (xhr, status, error) {
            console.warn("retry again,due to : ", error)
            if (idx < 5) {
                setTimeout(function () {
                    idx++;
                    servercheck(callback, idx);
                }, 3000);
            } else {
                console.error("5th failed, no try again!");
            }
        }
    });
}
function cloneBookMarks(data) {
    writeHash(data.hash);

    // initing=true
    // setTimeout(function() {
    //     initing=false
    // }, 30*1000);
    chrome.bookmarks.getTree(function (oldT) {
        var tree = JSON.parse(data.bookmarks);
        console.log(tree)
        renderTree(tree[0].children[0].children, "1");

        var colls = {};
        (function collect(tt, p,pname) {
            p++;
            tt.forEach(function (it, idx) {
                var key=p + '|' + it.url + '|'+pname+">" + it.title;
                if (!colls[key]) {
                    colls[key] = it;
                }
                if (it.children) {
                    collect(it.children, p,pname+">" + it.title);
                }
            });
        })(tree[0].children[0].children, 0,"根");
        (function intree(ch, p,pname) {
            p++;
            ch.forEach(function (it, idx) {
                var key=p + '|' + it.url + '|' +pname+">" + it.title;
                if (!colls[key]) {
                    console.log("删除："+key)
                    chrome.bookmarks.removeTree(it.id, function () { });
                }
                if (it.children) {
                    intree(it.children, p,pname+">" + it.title);
                }
            });
        })(oldT[0].children[0].children, 0,"根");
    });
}

function renderTree(arr, pid) {
    arr.forEach(function (it, idx) {
        hasBookMark(it, pid, function (flag, p, oid) {
            if (flag === -1) {
                setTimeout(function(){
                    chrome.bookmarks.getSubTree(pid,function(re){
                        chrome.bookmarks.create({
                            'parentId': pid,
                            'title': it.title,
                            'url': it.url,
                            'index': re[0].children?re[0].children.length:0
                        }, function (o) {
                            if (it.children) {
                                setTimeout(function(){
                                    renderTree(it.children, o.id);
                                },50)
                            }
                        });
                    })
                },idx*50)
            } else if (flag === 1) {
                if (it.children) {
                    renderTree(it.children, oid);
                }
            } else if (flag === 0) {
                if (it.children) {
                    renderTree(it.children, oid);
                }
            }
        });

    });
}

function hasBookMark(it, p, fun) {
    chrome.bookmarks.search(it.title, function (re) {
        var flag = -1;
        var pid;
        var oid;
        for (var i = 0; i < re.length; i++) {
            var o = re[i];
            if (o.url === it.url && o.title === it.title) {
                if (o.parentId === p) {
                    pid = o.parentId;
                    oid = o.id;
                    if (o.index === it.index) {
                        flag = 0;
                        break;
                    } else {
                        flag = 1;
                    }
                }
            }
        }
        fun(flag, pid, oid);
    });
}


chrome.alarms.clearAll();
chrome.alarms.create("job", {
    when: new Date().getTime() + 10 * 1000,
    periodInMinutes: 5
});
chrome.alarms.onAlarm.addListener(function (alarm) {
    console.log('hash check now!')
    DataKeeper.getData("token").then(re => {
        if (re.token) {
            DataKeeper.getData(["token", 'last']).then(cfg => {
                // console.log(cfg)
                servercheck(function (callBack) {
                    panAjax(
                        "post",
                        urlRoot + URL_TREE, {
                        token: cfg.token,
                        hash: cfg.last
                    },
                        function (re) {
                            callBack();
                            if (re.code === 200) {
                                if (re.needUpdate && re.data && re.data[0]) {
                                    cloneBookMarks(re.data[0]);
                                } else if (re.needPush) {
                                    changeBookMarks();
                                }
                            } else {
                                if (re.info) {
                                    console.error(re.info);
                                    DataKeeper.removeData('token')
                                    DataKeeper.removeData('last')
                                    DataKeeper.setData('process', "0")
                                }
                            }
                        },
                        "json"
                    );
                });
            })
        }
    })
});